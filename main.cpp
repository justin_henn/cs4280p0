//main
//This is the main file for the binary search tree
//Justin Henn
//Assignment 0
//2/15/18
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include "tree.h"
#include "node.h"
#include "traversal.h"

using namespace std;

int main(int argc, char **argv) {

/*Variables needed from main*/

  ifstream myfile;
  char arg1[1024];
  const char ADD_ON[] = ".sp18";
  string filename = "";
  
/*Check if file argument*/

  if (argc > 1) {



    strcpy(arg1, argv[1]);
    strcat(arg1, ADD_ON);
    myfile.open(arg1);
  }

  /*If no file argument*/

  else {

    ofstream ofile;
    ofile.open("input.txt");
    string s;

    if (!ofile.is_open()) {
 
      cout << "Unable to open file\n";
      return 0;
  }

    while(getline(cin, s)) {

      ofile << s << "\n";

    }
    ofile.close();
    myfile.open("input.txt");

  }

 /*check if file is not empty*/

  if (!myfile.is_open()) {
  
    cout << "Unable to open file\n";
    return 0;
  }

  myfile.seekg(0, myfile.end);
  int file_size = myfile.tellg();
  myfile.seekg(0, myfile.beg);

  if (file_size == 0) {

    cout << "Error, empty file\n";
    return 0;
  }
 
  /*get output file names*/

  if (argc > 1) {

    filename = argv[1];
  }
  else {

    filename = "out";
  }

  /*do tree operations*/
  
  node_t *root = NULL;
  root = buildTree(root, myfile);
  parseAll(root, 0, filename);
  myfile.close();
 }
