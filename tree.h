//tree.h
//This is the tree h file
//Justin Henn
//Assignment 0
//2/15/18
#ifndef TREE_H
#define TREE_H

#include "node.h"

node_t* buildTree(node_t* node, ifstream &myfile);

#endif
