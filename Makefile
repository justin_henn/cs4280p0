CC	= g++		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= P0
OBJS	= main.o tree.o traversal.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.cpp tree.h node.h
	$(CC) $(CFLAGS) -c main.cpp

tree.o: tree.cpp tree.h node.h
	$(CC) $(CFLAGS) -c tree.cpp

traversal.o: traversal.cpp traversal.h node.h
	$(CC) $(CFLAGS) -c traversal.cpp
clean:
	/bin/rm -f *.txt *.sp18 *.o *~ *.preorder *.inorder *.postorder $(TARGET)
