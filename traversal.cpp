//traversal
//This is the traversal program for traversing and printing the tree
//Justin Henn
//Assignment 0
//2/15/18
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "node.h"
#include "traversal.h"

using namespace std;

/*print tree in post order*/

void postOrder(node_t* node, int level, FILE* postfile) {

  if (node == NULL)
    return;

  string string_total = "";
  postOrder(node->left, level+1, postfile);

  postOrder(node->right, level+1, postfile);

  for (int i = (node->different_strings).size()-1; i >= 0; i--) {

    string_total += (node->different_strings)[i];
  }
  fprintf(postfile,"%*c%d:%-9s\n", level*2,' ',node->key,string_total.c_str());

}

/*print tree in order*/

void inOrder(node_t* root, int level, FILE* infile) {

  if (root == NULL) 
    return;

    string string_total = "";
    inOrder(root->left, level+1, infile);

    for (int i = (root->different_strings).size()-1; i >= 0; i--) {

      string_total += (root->different_strings)[i];
    }
    fprintf(infile,"%*c%d:%-9s\n", level*2,' ',root->key,string_total.c_str());
    inOrder(root->right, level+1,infile);
}

/*print tree in pre order*/

void preOrder(node_t* node, int level, FILE * prefile) {

  if (node == NULL)
    return;

  string string_total = "";

  for (int i = (node->different_strings).size()-1; i >= 0; i--) {

    string_total += (node->different_strings)[i];
  }

  fprintf(prefile,"%*c%d:%-9s\n", level*2,' ',node->key,string_total.c_str());

  preOrder(node->left, level+1, prefile);

  preOrder(node->right, level+1, prefile);
}

/*function to start parsing process*/

void parseAll(node_t* node, int level, string filename) {

  string in_filename = filename + ".inorder";
  string pre_filename = filename + ".preorder";
  string post_filename = filename + ".postorder";

  FILE * infile;
  FILE * prefile;
  FILE * postfile;
  
  if ((infile = fopen(in_filename.c_str(), "w")) == NULL) {

    printf("Cannot open file.\n");
    return;
  }

  if ((prefile = fopen(pre_filename.c_str(), "w")) == NULL) {

    printf("Cannot open file.\n");
    return;
  }

  if ((postfile = fopen(post_filename.c_str(), "w")) == NULL) {

    printf("Cannot open file.\n");
    return;
  }

  postOrder(node, level, postfile);
  preOrder(node, level, prefile);
  inOrder(node, level, infile);

  fclose(infile);
  fclose(postfile);
  fclose(prefile);

}
