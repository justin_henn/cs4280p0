//node
//This is the node h file
//Justin Henn
//Assignment 0
//2/15/18
#ifndef NODE_H
#define NODE_H
#include <vector>
#include <iostream>

using namespace std;

 typedef struct node_t{

  int key;
  node_t *left, *right;
  vector<string> different_strings;

}node_t;
  
#endif
