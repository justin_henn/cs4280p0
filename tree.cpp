//tree.c
//This is the tree file for creating the tree
//Justin Henn
//Assignment 0
//2/15/18
#include <fstream>
#include "node.h"
#include "tree.h"

/*create a new node*/

node_t* newNode(int item, string str) {

  node_t *temp = new node_t;
  temp->key = item;
  (temp->different_strings).push_back(str);
  temp->left = temp->right = NULL;
  return temp;
}


/*insert a node*/

node_t* insert( node_t* node, int key, string str) {

  if (node == NULL) return newNode(key, str);

  if (key == node->key) {

    string new_str = str + " ";
    int is_same = 0;
    for (int i = 0; i < (node->different_strings).size(); i++) {

      if ((node->different_strings)[i] == new_str)
	is_same = 1;
    }
      if (is_same == 0)
	(node->different_strings).push_back(new_str);
  
      return node;
  }

  if (key < node->key) {
   
    node->left = insert(node->left, key, str);
  }
  else
    node->right = insert(node->right, key, str);

  return node;
}

/*start build of tree*/

node_t* buildTree( node_t* node, ifstream &myfile) {

  string str;
  node_t* root = node;

  myfile >> str;
  root = insert(root, str.length(), str);

  while(myfile >> str){

    insert(root, str.length(), str);
  }

  return root;
}




